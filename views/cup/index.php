<body>   
<?php
if(empty($_SESSION['user'])):
?>
    <section class="jumbotron text-center">
            <div class="container">
                <p><small>After you authorize you will be able to tag each of pictures</small></p>                  
            </div>
    </section>
    
<?php
endif;
?>    
    <div class="album py-5 bg-light">
            <div class="container">
                    <form action="GET">
                        <input  name="search" class="form-control form-group" placeholder="You can search images by tags">
                    </form>
                    <div class="row">
<?php 
if(!empty($pictures)):
    foreach ($pictures as $picture):
   
?>
                            <div class="col-md-3 col-sm-3 picgrid">
                                <div class="card mb-3  box-shadow"><img id="<?=$picture['resource_id']?>" class="card-img-top" onclick="showPicture(<?=$picture['picture_id']?>);return true;" src="<?=$picture['url']?>" data-holder-rendered="true">
                                            <div class="card-body">
                                                    <p class="card-text text-center"><?=$picture['name']?></p>
                                            </div>
                                    </div>
                            </div>
<?php 
    endforeach;
endif;
?>

                    </div>
            </div>
    </div>
   
    
    <div class="modal  modal-img fade bd-example-modal-lg"  aria-hidden="true"  style="visibility: hidden!important;">
        <div class="modal-dialog modal-lg"  style="visibility: hidden!important;">
            <div width="1px" height="1px" class=" modal-content modal-content-img" >
      
            </div>
        </div>
    </div>

</body>    