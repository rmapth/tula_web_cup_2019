<header>                                            
        <div class="navbar navbar-dark bg-dark box-shadow">
                <div class=" d-flex justify-content-between">
                        <a href="/" class="navbar-brand d-flex align-items-center">
                            <strong>Tula Web Cup 2019</strong> 
                        </a>                        
                </div>
                <a id="login" class="btn btn-outline-primary" href="#">Sign up</a>
        </div>
</header>

<div id="login-modal" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div>
                
                <div class="card">
                        <article class="card-body"><!-- <a href="" class="float-right btn btn-outline-primary">Sign up</a>-->
                                <h4 class="card-title mb-4 mt-1">Sign in</h4>
                                <p>
                                        <a id="yandex-passport-authorization" href="https://oauth.yandex.ru/authorize?response_type=code&client_id=<?=YANDEX_ID?>"   class="btn btn-block btn-outline-primary btn-yandex-passport"> <i class="fab fa-yandex"></i> &nbsp; Login via Yandex.Passport</a>
                                       
                                </p>
                                <hr>

                        </article>
                </div>
            </div>
        </div>
    </div>
</div>