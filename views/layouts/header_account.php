<header>                                            
        <div class="navbar navbar-dark bg-dark box-shadow">
                <div class="d-flex justify-content-between ">
                        <a href="/" class="navbar-brand d-flex align-items-center">
                            <strong>Tula Web Cup 2019</strong> 
                        </a>                        
                </div>
                <div class="dropdown">
                    <span class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        Welcome, <?= $_SESSION['display_name']   ?>
                    </span>
                    <div class="dropdown-menu"> <a class="dropdown-item" href="/logout">Exit</a> </div>
                </div>    
        </div>
</header>
