<?php

class SqlPDO{
    
    private $host = 'localhost';
    private $db   = 'tkache7a_jira';
    private $user = 'tkache7a_jira';
    private $pass = 'issuesissue';
    private $charset = 'utf8';
    private $pdo ;
    
    public function __construct() {
       
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $this->user, $this->pass,$opt);
    }

    public function select($sql,$data=[]){
        $presence = $this->pdo->prepare($sql);
        if(empty($data)){
             $presence->execute($data);    
        }else{
            $presence->execute();    
        }
        return $presence;
    }
}