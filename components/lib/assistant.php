<?php

function pdoSet($allowed, $source ) {
  $set = '';
  $values = array();
  if (empty($source)) return false ;
  foreach ($allowed as $field) {
    if (isset($source[$field])) {
      $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
      $values[$field] = $source[$field];
    }
  }
  return substr($set, 0, -2); 
}

function logger_append($message,$name='processing'){
    $path = __DIR__.'/log/'.$name.'.log';      
    file_put_contents($path,$message,FILE_APPEND);   
}
function logger($message,$name='date'){
    $path = __DIR__.'/log/'.$name.'.log'; 
    
    file_put_contents($path,$message."\r\n");   
} 