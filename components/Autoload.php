<?php
spl_autoload_register(function ($class) {
    $paths=array(
        '/models/',
        
        '/components/'
    );
foreach ($paths as $path){
   $path = __DIR__.'/..'.  $path . $class.'.php';
   if (is_file($path))
   {
       include_once $path;  
       break;
   }
   
}
require_once  __DIR__.'/..'.'/controllers/SiteController.php';

});
