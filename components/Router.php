<?php

class Router
{

    private $routes;

    public function __construct()
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * Returns request string
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {          
            return explode('?',trim($_SERVER['REQUEST_URI'], '/'))[0];
        }
    }

    public function run()
    {
         try{
            try{
                // Получить строку запроса
                $uri = $this->getURI();

                // Проверить наличие такого запроса в routes.php
                foreach ($this->routes as $uriPattern => $path) {
                    // Сравниваем $uriPattern и $uri
                    if (preg_match("~$uriPattern~", $uri)) {

                        // Получаем внутренний путь из внешнего согласно правилу.
                        $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                        // Определить контроллер, action, параметры

                        $segments = explode('/', $internalRoute);

                        $controllerName = array_shift($segments) . 'Controller';
                        $controllerName = ucfirst($controllerName);


                        $actionName = 'action' . ucfirst(array_shift($segments));

                        $parameters = $segments;

                        // Подключить файл класса-контроллера
                        $controllerFile = ROOT . '/controllers/' .
                                $controllerName . '.php';

                        if (file_exists($controllerFile)) {
                            include_once($controllerFile);
                             // Создать объект, вызвать метод (т.е. action)
                            $site = new SiteController();       
                            $controllerObject = new $controllerName;       
                           
                            if(method_exists($controllerObject,$actionName)){
                                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);                        

                                if(empty($result) || !$result){
                                    $result = call_user_func_array(array($site, 'action404'), $parameters);                               
                                }elseif(!empty($result['error'])){
                                    $result = call_user_func_array(array($site, 'action500'), $parameters); 
                                }
                                break;
                            
                            }else{
                                $obj = new SiteController();                            
                                $result = call_user_func_array(array($obj, 'action404'), $parameters);      
                            }
                        }else{
                            $obj = new SiteController();                            
                            $result = call_user_func_array(array($obj, 'action404'), $parameters);   
                        }  
                    break;
                    }
                }
            }
            catch (\Exception $ee){
                echo '<pre>';
                var_dump($ee);                   
                echo '</pre>';
            }  
        } catch (Error $e){
            echo '<pre>';
            var_dump($e);
            echo '</pre>';
        }            
    }
}
