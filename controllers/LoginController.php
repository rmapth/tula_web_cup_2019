<?php


class LoginController {
    public function actionYandex(){
        if( !empty($_SESSION['user'])){
            header("Location: http://rmapth.beget.tech/");
        } 
        
        if(!empty($_GET['code'])){
            $code = filter_input(INPUT_GET, 'code');
            $token = \Login::yandexAuthonticationToken($code);
        
            if(!empty($token)){
                $info = \Login::yandexAuthonticationInfo($token);
                             
                if(!empty($info)){
                    $user = \Login::authorizationInfoFromYandex($info);
                    if(empty($user['id'])){
                        header("Location: http://rmapth.beget.tech/");
                    }else{
                        $_SESSION['user'] = $user['id'];
                        
                        $_SESSION['display_name'] = (empty($user['display_name']))? $user['first_name']:$user['display_name'];
                         header("Location: http://rmapth.beget.tech/");
                    }                    
                }
            }
        }else{
           header("Location: http://rmapth.beget.tech/");
        }
        
        return true;
    }
    
    public function actionLogout(){
        session_destroy();               
        header("Location: http://rmapth.beget.tech/");
        return true;// just in case
    }
}
