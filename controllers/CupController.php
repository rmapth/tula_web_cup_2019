<?php


/**
 * Description of CupController
 *
 * @author Rmapth
 */
class CupController {
    
    
    private $app;
     private $Media;
    function __construct() {
        $this->app = new Rmapth\app\app($this);
        $this->Media = new Media();
    }
    public function actionIndex(){   
         try{
            $limit = (empty($_GET['limit']))?80:(int)$_GET['limit'];
            $page = (empty($_GET['page']))?0:(int)$_GET['page'];
            $pictures = $this->Media->getPictures($limit,$page);

            $this->app->open('Главная');             
            $this->app->header();    
            
            require ROOT.'/views/cup/index.php'; // This is rediculous, but the right way on line below  is not workong. I should fix it later.
//            $this->app->render('index');
            

            $this->app->close();
            return true;
        } catch (Exception $exception){
            return [
                'error'=>$exception
            ];
        } catch (Error $error){
            return [
                'error'=>$error
            ];
        }        
        return true;
    
    }
}
