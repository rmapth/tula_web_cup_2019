<?php

/**
 * Контроллер SiteController
 */
class SiteController
{

    private $app;
    
    function __construct() {
        $this->app = new Rmapth\app\app($this);
    }
    
    public function action404(){
        $this->app->open('404 – страница не найдена');
        $this->app->render('404');
    }

    public function action500(){
        try{
            $this->app->open('500 - внутренняя ошибка сервера');
            $this->app->render('500');
        }catch(Exception $exception){
            return [
                'error'=>$exception
            ];
        }
        catch(Error $error){
            return [
                'error'=>$error
            ];
        }
        
        return true;
    }

    /** 
     * 
     * @return boolean
     */
    public function actionIndex()
    {   
        try{
            //head
            $this->app->open('Главная'); 
            
            $this->app->header();
            
           
            $this->app->render('index');
            

            $this->app->close();
            return true;
        } catch (Exception $exception){
            return [
                'error'=>$exception
            ];
        } catch (Error $error){
            return [
                'error'=>$error
            ];
        }        
    }
    
    public function actionLogin()
    { 
        header('Access-Control-Allow-Origin: *');      

        if(!empty($_GET['action'])){
           
            switch ($_GET['action']){
                case 'login':
                    $Account = new Account();
                    if(!empty($_POST['data']['account']) && !empty($_POST['data']['pass'])){
                        $safePost = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                        $condition = [
                            'email'=>$safePost['data']['account'],
                            'password'=>md5($safePost['data']['pass'])
                        ];

                        $client = $Account->getClient($condition);
                        header("Content-type: application/json; charset=utf-8");
                        if(!empty($client)){
                            
                            $_SESSION['account'] = $client[0]['id'];
                            echo json_encode([
                                'success'=> true,

                            ],JSON_UNESCAPED_UNICODE);

                        }else{

                            echo json_encode([
                                'success'=> false,
                                'answer'=>'Не найдено'
                            ],JSON_UNESCAPED_UNICODE);

                        }
                    }

                    return true;
                case 'logout':
                    session_destroy();
                    header('Location: http://rmapth.beget.tech/ ');

                    return true;


                default :
                    return true;
            }
        }else{
            $this->app->open();
            if(empty($_SESSION['account'])){
                require ROOT.'/views/site/login.php';
                require ROOT.'/views/layouts/footer.php';
            }else{

                require ROOT.'/views/site/start.php';
                require ROOT.'/views/layouts/footer.php';
            }
            return true;
        }
    }
}
