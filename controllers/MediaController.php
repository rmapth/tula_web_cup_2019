<?php

/**
 * 
 */
class MediaController{
    private $media;
    public function __construct() {
        $this->media = new Media;
        
    }

    public function actionPicture($id){
       if(empty($_POST['access'])){
           return false;
       }
       
        $limit = (empty($_GET['limit']))?80:(int)$_GET['limit'];
        $page = (empty($_GET['page']))?0:(int)$_GET['page'];
        
       
        $media = new Media;
        $picture = $media->getPicture($id);
        $next = $this->media->getNextId($id);
       
       
        if(empty($next)){
             $next = $this->media->getMinId();
        }

        $prev = $this->media->getPrevId($id);
       
        if(empty($prev)){
             $prev = $this->media->getMaxId();
        }

?>

<div  width="1px" height="1px" class="slider" style="visibility: hidden!important">
    <ul>
        <li id="no-js-slider-1" class="slide" style="visibility: visible!important">
            <img style="width: 750px;height: 500px; top: 5px; left: -10px" src="<?=$picture['url']?>">
            <a class="prev" onclick="showPicture(<?=$prev[0]['picture_id']?>);return true;">prev</a>
            <a class="next" onclick="showPicture(<?=$next[0]['picture_id']?>);return true;">next</a>
        </li>  
    </ul>
</div>


<?php
        return true;
    }
    
    public function actionSyncronize(){       
        $result = $this->media->syncronizePictures();       
    }
    
}