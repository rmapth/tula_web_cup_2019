<?php


// 1. Общие настройки
//В тестовом режиме
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

session_start();

if (!empty($_POST['logout'])) {
    session_destroy();
     header('Location: http://rmapth.beget.tech/ ');
}

// 2. Подключение файлов системы
define('ROOT', dirname(__FILE__));

require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/components/Autoload.php';
require_once __DIR__.'/components/Router.php';
require_once __DIR__.'/config/config.php';

use Rmapth\DB\DB; 
use Rmapth\app\app; 

// 4. Вызов Router
$router = new Router();
$router->run();

?>
