<?php

declare(strict_types=1);

namespace Testirovanie\SberbankAcquiring\Exception;

/**
 * Action exception.
 *
 * @author Oleg Voronkovich <oleg-voronkovich@yandex.ru>
 */
class ActionException extends SberbankAcquiringException
{
}
