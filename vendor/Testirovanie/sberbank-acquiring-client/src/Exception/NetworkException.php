<?php

declare(strict_types=1);

namespace Testirovanie\SberbankAcquiring\Exception;

/**
 * Network exception.
 *
 * @author Oleg Voronkovich <oleg-voronkovich@yandex.ru>
 */
class NetworkException extends SberbankAcquiringException
{
}
