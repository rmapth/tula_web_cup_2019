<?php

declare(strict_types=1);

namespace Testirovanie\SberbankAcquiring\Exception;

/**
 * @author Oleg Voronkovich <oleg-voronkovich@yandex.ru>
 */
class ResponseParsingException extends SberbankAcquiringException
{
}
