<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of db
 *
 * @author im-business3
 */

namespace Imbusiness\DB;

use Imbusiness\config\config;

class DB extends config {
    
    private $pdo ;
    private $accounts_table; 
    public function __construct() {
        $config = $this->getConfig('db');
        $charset = 'utf8';

        $dsn = "mysql:host=".$config['host'].";dbname=".$config['db'].";charset=$charset";
        $opt = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new \PDO($dsn, $config['user'], $config['password'], $opt);
        $this->accounts_table = $this->getConfig('accounts_table');
        
    }
    public function decreasePeriod(){      
        $sql = "UPDATE accounts_details SET period = period - 1 WHERE period>0";
        $query = $this->pdo->prepare($sql);           
        return $query->execute([]);                     
    }
    
    public function setActive($conditions){      
        if(!is_array($conditions)){
            return FALSE;
        }elseif(is_array($conditions)){
            $keys = array_keys($conditions);       
            $values = array_values($conditions);
            $sql = "UPDATE accounts SET ".$this->pdoSet($keys,$conditions);
            $query = $this->pdo->prepare($sql);           
            return $query->execute($values);                
        }        
        return true;                    
    }
    public function addAccount($conditions){
        if(!is_array($conditions)){
            return FALSE;
        }elseif(is_array($conditions)){
            $keys = array_keys($conditions);       
            $values = array_values($conditions);
            $sql = "INSERT INTO accounts SET ".$this->pdoSet($keys,$conditions);
            $query = $this->pdo->prepare($sql);           
            return $query->execute($values);                
        }        
        return true;
    }
    public function getDeals($conditions){
        if(!is_array($conditions)){
            return FALSE;
        }elseif(is_array($conditions)){
            $keys = array_keys($conditions);       
            $values = array_values($conditions);

            $sql = "SELECT * FROM deals WHERE ".$this->pdoSetSelect($keys,$conditions);
            $query = $this->pdo->prepare($sql);   
            $query->execute($conditions);
            return $query->fetchAll();                
        }        
        return true;
    }
    
    
    
    public function addDeal($conditions){
        if(!is_array($conditions)){
            return FALSE;
        }elseif(is_array($conditions)){
            $keys = array_keys($conditions);       
            $values = array_values($conditions);
            $sql = "INSERT INTO deals SET ".$this->pdoSet($keys,$conditions);
            $query = $this->pdo->prepare($sql);           
            return $query->execute($values);                
        }        
        return true;
    }
    
    public function addAccountsDetails($conditions){
        if(!is_array($conditions)){
            return FALSE;
        }elseif(is_array($conditions)){
            $keys = array_keys($conditions);       
            $values = array_values($conditions);
            $sql = "INSERT INTO accounts_details SET ".$this->pdoSet($keys,$conditions);
            $query = $this->pdo->prepare($sql);           
            return $query->execute($values);                
        }        
        return true;
    }
    
        
    public function getAccountByUrl($url){
        $query = $this->pdo->prepare("SELECT * FROM $this->accounts_table WHERE retailAPIURL = ? LIMIT 1");
        $query->execute([$url]);
        return $query->fetchAll();
    }
    
    public function getAccount($conditions){
        if(!is_array($conditions) && is_string($conditions)){
            
            $query = $this->pdo->prepare("SELECT * FROM $this->accounts_table WHERE " . $conditions);
            $query->execute();

            return $query->fetchAll();
        }elseif(is_array ($conditions))
        {
            $keys = array_keys($conditions);       
            $values = array_values($conditions);
            $sql = "SELECT * FROM $this->accounts_table WHERE " . $this->pdoSetSelect($keys,$conditions);
            $query = $this->pdo->prepare($sql);           
            $query->execute($values);            
            return $query->fetchAll();
        }else{
            return false;
        }
    }
    public function getAccountsDetails($conditions){
        if(!is_array($conditions) && is_string($conditions)){
            
            $query = $this->pdo->prepare("SELECT * FROM accounts_details WHERE " . $conditions);
            $query->execute();

            return $query->fetchAll();
        }elseif(is_array ($conditions))
        {
            $keys = array_keys($conditions);       
            $values = array_values($conditions);
            $sql = "SELECT * FROM accounts_details WHERE " . $this->pdoSet($keys,$conditions);
            $query = $this->pdo->prepare($sql);           
            $query->execute($values);            
            return $query->fetchAll();
        }else{
            return false;
        }
    }
    public function getAccountWithDetails($conditions){
        $data['account'] = [];
        $data['accounts_details'] = [];
        
        $data['account'] = $this->getAccount($conditions);
        if(!empty($data['account'])){          
            $data['account'] = $data['account'][0];
            $data['accounts_details'] = $this->getAccountsDetails(['uniqueAccountHash'=>$data['account']['uniqueAccountHash']]);
        }
        return $data;
        
    }

    public function getAccountsWithDetails(){
        $sql = "SELECT * from accounts LEFT JOIN accounts_details ON accounts.uniqueAccountHash = accounts_details.uniqueAccountHash ";
        $query = $this->pdo->prepare($sql);           
        $query->execute([]);            
        return $query->fetchAll();        
    }        
    function pdoSet($allowed, $source) {
      $set = '';
      $values = array();
      if (empty($source)) return false ;
      foreach ($allowed as $field) {
        if (isset($source[$field])) {
          $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
          $values[$field] = $source[$field];
        }
      }
      return substr($set, 0, -2); 
    }
    function pdoSelect($allowed,$source){
        $set = '';
        $values = array();
        if (empty($source)) return false ;
        foreach ($allowed as $field) {
          if (isset($source[$field])) {
            $set.="`".str_replace("`","``",$field)."`". "=:$field AND ";
            $values[$field] = $source[$field];
          }
        }
        $set = trim($set, 'AND ');
        return $set; 
    }

}
