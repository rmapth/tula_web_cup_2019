<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Rmapth\app;
class app {
    private $className = '';
    
    public function __construct($class) {        
        $this->className = strtolower(str_replace('Controller','',get_class($class)));
    }
    
    public function open($title='Rmapth'){
        require_once __DIR__.'/../views/layouts/head.php';
    }
    
    public function close(){
         require_once __DIR__.'/../views/layouts/foot.php';
    }
    public function header(){
        if(empty($_SESSION['user'])){
            require_once __DIR__.'/../views/layouts/header_login.php';
        }else{
            require_once __DIR__.'/../views/layouts/header_account.php';
        }
    }
    
    public function render($file){
        if(!is_string($file)){
            return false;
        }
        try{
            require_once __DIR__."/../views/{$this->className}/{$file}.php";
        }
        catch (ERROR $e){
            return false;
        }
    }
}
