<?php

class Debug
{
    private $logfile = '';

    public function __construct($fileName = 'log.txt')
    {
         $this->logfile = '../logs/' . $fileName . '_' . date('Y-m-d') . '.log';
    }

    public static function trace($arg = null, $die = false)
    {
        if (!$arg)
        {
            $arg = '';
        }
        $arg = print_r($arg, true);
        echo '<pre>' . $arg . '</pre>';
        if ($die)
        {
            die();
        }
    }

    public function log($arg = null, $die = false)
    {
        if (!$arg)
        {
            return false;
        }
        if (!is_string($arg))
        {
            $arg = print_r($arg, true);
        }
        file_put_contents($this->logfile, $arg . PHP_EOL, FILE_APPEND);
        if ($die)
        {
            die('die');
        }
        return true;
    }
}