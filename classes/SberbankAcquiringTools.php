<?php
/**
 * class SberbankAcquiringTools
 *
 * вспомогательные методы
 *
 */

namespace Imbusiness\SberbankAcquiring;

use Exception;
use RetailCrm\ApiClient;
use RetailCrm\Exception\CurlException;
use Voronkovich\SberbankAcquiring\OrderStatus;


class SberbankAcquiringTools
{
    /**
     * обязательные поля запроса
     *
     * @var array
     */
    public static $needRequestFields = [
        'orderId' => 'orderId is empty',
        'orderTotalSum' => 'orderTotalSum is empty',
        'crmApiUrl' => 'crmApiUrl is empty',
        'crmApiKey' => 'crmApiKey is empty',
        
    ];

    /**
     * проверим пришедшие данные из CRM на обязательность
     *
     * @return mixed
     * @throws Exception
     */
    public static function checkRequestFromCrm()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST')
        {
            throw new Exception('Error: The request must use the POST method');
        };

        $request = $_POST;

        if (empty($request))
        {
            throw new Exception('Error: The request is empty');
        }

        if (
            empty($request['sberApiUserName']) && empty($request['sberApiPassword']) && empty($request['sberApiToken'])
        )
        {
            throw new Exception('Error: sberApiUserName and sberApiPassword or sberApiToken is empty');
        }

        $errorList = [];
        foreach (self::$needRequestFields as $fieldKey => $fieldErrorMess)
        {
            if (empty(trim($request[$fieldKey])))
            {
                $errorList[] = $fieldErrorMess;
            }
        }

        if (!empty($errorList))
        {
            throw new Exception('Error: require request parameters is empty.'
                . PHP_EOL . implode(PHP_EOL, $errorList));
        }

        return $request;
    }

    /**
     * сформируем из запроса CRM массив с нужными данными
     *
     * @param $request array
     * @return array
     */
    public static function getDataFromRequest($request)
    {
        $data = [

            // params from Sberbank API - https://developer.sberbank.ru/doc/v1/acquiring/rest-requests-about
            'sberAPI' => [
                'userName' => $request['sberApiUserName'], // your UserName
                'password' => $request['sberApiPassword'], // your Password
                'token' => $request['sberApiToken'], // your token
            ],

            // params from customer
            'customer' => [
                'email' => $request['customerEmail'],
                //'phone' => '89000000000', // ???
            ],

            // params from order
            'order' => [
                'id' => $request['orderId'],
                'amount' => $request['orderTotalSum'] * 100,
            ],

            // params from crm API
            'crmAPI' => [
                'url' => $request['crmApiUrl'],
                'key' => $request['crmApiKey'],
            ],

            'errorMail' => (!empty($request['errorReportEmail'])) ? $request['errorReportEmail'] : 'sterg.work@gmail.com',

            'callbackUrl' => "http://sberbank-acquiring.imb-service.ru/payment/callback",

        ];

        return $data;
    }

    /**
     * проверим пришедшие данные из CRM на обязательность
     *
     */
    public static function checkRequestFromSber()
    {
        $request = $_GET;

        if (empty($request))
        {
            throw new Exception('Error: The request is empty');
        }

        if (empty($request['orderId']))
        {
            throw new Exception('Error: The order is empty');
        }

        return $request;
    }

    /**
     * сформируем из запроса CRM массив с нужными данными
     *
     * @param $request
     * @return array
     */
    public static function getDataFromRequestSber($request)
    {
        $data = [

            // params from Sberbank API - https://developer.sberbank.ru/doc/v1/acquiring/rest-requests-about
            'sberAPI' => [
                'userName' => ((defined('SBER_API_USERNAME'))) ? SBER_API_USERNAME : '', // your UserName
                'password' => ((defined('SBER_API_PASSWORD'))) ? SBER_API_PASSWORD : '', // your Password
                'token' => ((defined('SBER_API_TOKEN'))) ? SBER_API_TOKEN : '', // your token
            ],

            // params from order
            'order' => [
                'sberId' => $request['orderId'],
            ],

            // params from crm API
            'crmAPI' => [
                'url' => CRM_API_URL,
                'key' => CRM_API_KEY,
            ],

//            'success' => $request['Success'],
//            'status' => $request['Status'],
            'errorMail' => defined('ERROR_REPORT_EMAIL') ? ERROR_REPORT_EMAIL : 'sterg.work@gmail.com',
        ];

        return $data;
    }

    /**
     * $request array
     * @example:
     * [orderId] => 4141b97d-37cf-7d1b-a4c7-296604b1a3b3
     * [lang] => ru
     *
     * @param $statusSber
     * @param $orderId
     * @throws Exception
     */
    public static function sendStatusToCrm($statusSber, $orderId)
    {
        $syncStatusSberToCrm = [
            OrderStatus::DEPOSITED => 'paid',       // 2 - проведена полная авторизация суммы заказа - Оплачен
            OrderStatus::REVERSED => 'not-paid',    // 3 - авторизация отменена - Не оплачен
            OrderStatus::REFUNDED => 'not-paid',    // 4 - по транзакции была проведена операция возврата - Не оплачен
            OrderStatus::DECLINED => 'fail',        // 6 - авторизация отклонена - Ошибка
        ];

        $statusCrm = '';
        if (!empty($syncStatusSberToCrm[$statusSber]))
        {
            $statusCrm = $syncStatusSberToCrm[$statusSber];
        }

        if (empty($statusCrm))
        {
            $GLOBALS['im-business']['log']->log('Error: not found $statusCrm');
            throw new Exception('Error: not found $statusCrm');
        }

        $paymentId = self::getPaymentId($orderId);

        if (empty($paymentId))
        {
            $GLOBALS['im-business']['log']->log('Error: not found $paymentId');
            throw new Exception('Error: not found $paymentId');
        }

        $clientApiCrm = new ApiClient(
            $GLOBALS['im-business']['data']['crmAPI']['url'],
            $GLOBALS['im-business']['data']['crmAPI']['key'],
            ApiClient::V5
        );

        try
        {
            $payment = [
                'id' => $paymentId,
                'status' => $statusCrm,
                'comment' => 'SberbankAcquiring API send:'
                    . PHP_EOL . ' status - "' . $statusCrm . '" ' . date('d.m.Y H:i:s'),
                'paidAt' => date('Y-m-d H:i:s'),
                //'externalId' => $sberPaymentId,
            ];
            $crmResponse = $clientApiCrm->request->ordersPaymentEdit($payment, 'id');

            $GLOBALS['im-business']['log']->log('$order: '. print_r($payment, 1));
            $GLOBALS['im-business']['log']->log('$crmResponse: '. print_r($crmResponse, 1));
        }
        catch (CurlException $e)
        {
            $GLOBALS['im-business']['log']->log("Connection error: " . $e->getMessage());
        }

        if ($crmResponse->isSuccessful())
        {
            $GLOBALS['im-business']['log']->log('isSuccessful - ' . $crmResponse->getStatusCode());
        }
        else
        {
            $error = sprintf(
                "Error: [HTTP-code %s] %s",
                $crmResponse->getStatusCode(),
                $crmResponse->getErrorMsg()
            );

            if (isset($crmResponse['errors']))
            {
                $error .= print_r($crmResponse['errors'], 1);
            }

            $GLOBALS['im-business']['log']->log('Error: not success in crm response - ' . $error);
            throw new Exception('Error: not success in crm response - ' . $error);
        }
    }

    public static function getPaymentId($orderId)
    {
        $paymentId = 0;

        $clientApiCrm = new ApiClient(
            $GLOBALS['im-business']['data']['crmAPI']['url'],
            $GLOBALS['im-business']['data']['crmAPI']['key'],
            ApiClient::V5
        );

        try
        {
            $crmResponse = $clientApiCrm->request->ordersGet($orderId, 'id');
            $GLOBALS['im-business']['log']->log('$crmResponse: '. print_r($crmResponse, 1));
        }
        catch (CurlException $e)
        {
            $GLOBALS['im-business']['log']->log("Connection error: " . $e->getMessage());
        }

        if ($crmResponse->isSuccessful())
        {
            $GLOBALS['im-business']['log']->log('isSuccessful - ' . $crmResponse->getStatusCode());
        }
        else
        {
            $error = sprintf(
                "Error: [HTTP-code %s] %s",
                $crmResponse->getStatusCode(),
                $crmResponse->getErrorMsg()
            );

            if (isset($crmResponse['errors']))
            {
                $error .= print_r($crmResponse['errors'], 1);
            }

            $GLOBALS['im-business']['log']->log('Error: not success in crm response - ' . $error);
            throw new Exception('Error: not success in crm response - ' . $error);
        }

        $paymentList = $crmResponse->order['payments'];
        foreach ($paymentList as $payment)
        {
            if ($payment['type'] == 'sber-acquiring')
            {
                $paymentId = $payment['id'];
                break;
            }
        }

        return $paymentId;
    }
}