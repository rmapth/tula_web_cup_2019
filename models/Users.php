<?php

class Users extends DB{
    public function usersRegisterYandex($info){
        $data = [];
        if(!empty($info['first_name'])){
            $data ['first_name']=$info['first_name'];
        }
           
        if(!empty($info['last_name'])){
            $data ['last_name']=$info['last_name'];
        }   
          
        if(!empty($info['display_name'])){
            $data ['display_name']=$info['display_name'];
        } 
        
        if(!empty($info['default_email'])){
            $data ['email']=$info['default_email'];
        } 
        
        if(!empty($info['birthday'])){
            $data ['birthday']=$info['birthday'];
        } 
                        
        if(!empty($info['sex'])){
            $data ['sex']=$info['sex'];
        }
         
        if(!empty($info['id'])){
            $data ['yandex_id']=$info['id'];
        }
                          
        $allowed = array_keys($data);
        $sql = "INSERT INTO users SET ".$this->pdoSet($allowed, $data); 
        $query = $this->pdo->prepare($sql);
        $query->execute($data);
        
        return $this->findUser('',$this->pdo->lastInsertId());
        
    }
    
    public function findUser($email='',$id=''){
        
        if(!empty($email)){
        
            $sql = "SELECT * FROM users WHERE email = ?";

            $query = $this->pdo->prepare($sql);
            $query->execute([$email]);
            $result = $query->FETCH(PDO::FETCH_ASSOC);
            if(!empty($result)){
                return $result;
            }else{
                return false;
            }
        }
          if(!empty($id)){
        
            $sql = "SELECT * FROM users WHERE id = ?";

            $query = $this->pdo->prepare($sql);
            $query->execute([$id]);
            $result = $query->FETCH(PDO::FETCH_ASSOC);
            if(!empty($result)){
                return $result;
            }else{
                return false;
            }
        }
        
    }
    
}