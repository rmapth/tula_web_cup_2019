<?php

Class Album {
    public static function getPictures($limit=85,$offset=0){
        if(!is_int($limit)){
            $limit = 85;
        }
        
        $url ="https://cloud-api.yandex.net/v1/disk/resources/files?media_type=image&limit=".$limit;
    
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Authorization: OAuth ' . YANDEX_TOKEN]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $pictures = curl_exec($curl);
       
        curl_close($curl);
        return $pictures;
        
        
    }
}