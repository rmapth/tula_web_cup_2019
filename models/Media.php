<?php

class Media extends DB{
    public  function getPicture($id){
        $sql = "SELECT * FROM `pictures` WHERE picture_id = :id";
        $query = $this->pdo->prepare($sql);
        $query->bindParam(':id',$id);
        $query->execute();
        $result = $query->FETCH(PDO::FETCH_ASSOC);
     
        if(!empty($result)){
            return $result;
        }else{
            return false;
        }
        return ;
    }
    public  function getPictures($limit = 80,$offset = 0){
        if(!is_int($limit)){
            $limit = 80;
        }
        if(!is_int($offset)){
            $offset = 80;
        }
        $sql = "SELECT * FROM `pictures` WHERE 1 LIMIT {$limit} OFFSET {$offset}";
        $query = $this->pdo->prepare($sql);        
        $query->execute();
        $result = $query->FETCHALL(PDO::FETCH_ASSOC);
     
        if(!empty($result)){
            return $result;
        }else{
            return false;
        }
        return ;
    }
    public  function syncronizePictures(){
        
        $sql = "DELETE FROM `pictures` WHERE 1 ";
        $query = $this->pdo->prepare($sql);
        $result = $query->execute();
        
        $pictures  = json_decode(\Album::getPictures(80),true);
        if(empty($pictures) || !is_array($pictures)){
            return FALSE;
        }
        //loging is not needed for this small project but in big one 
        foreach ($pictures['items'] as $picture){
            $data = [
                'name'=>$picture['name'],
                'resource_id' => $picture['resource_id'],
                'url' => $picture['file'],
                
            ];
            $sql = "INSERT INTO pictures SET ".$this->pdoSet(array_keys($data), $data);
            $query = $this->pdo->prepare($sql);
            $result = $query->execute($data);
                       
        }
        return $result;
        
    }
    
    public function getMaxId(){
        $sql = "SELECT picture_id FROM `pictures` WHERE 1 ORDER BY picture_id DESC LIMIT 1 ";
        
        $query = $this->pdo->prepare($sql);
        $query->execute();
        
        $result = $query->FETCHALL(PDO::FETCH_ASSOC);
     
        if(!empty($result)){
            return $result;
        }else{
            return false;
        }
        return ;
        
    }
    
    public function getMinId(){
        $sql = "SELECT picture_id FROM `pictures` WHERE 1 ORDER BY picture_id ASC LIMIT 1 ";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        
        $result = $query->FETCHALL(PDO::FETCH_ASSOC);
     
        if(!empty($result)){
            return $result;
        }else{
            return false;
        }
        return ;
        
    }
    
     public function getNextId($id){
        $sql = "SELECT picture_id FROM `pictures` WHERE picture_id>{$id} ORDER BY picture_id ASC LIMIT 1 ";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        
        $result = $query->FETCHALL(PDO::FETCH_ASSOC);
     
        if(!empty($result)){
            return $result;
        }else{
            return false;
        }
        return ;
        
    }
     
     public function getPrevId($id){
        $sql = "SELECT picture_id FROM `pictures` WHERE picture_id<{$id} ORDER BY picture_id DESC LIMIT 1 ";
      
        $query = $this->pdo->prepare($sql);
        $query->execute();
        
        $result = $query->FETCHALL(PDO::FETCH_ASSOC);
     
        if(!empty($result)){
            return $result;
        }else{
            return false;
        }
        return ;
        
    }
}