<?php
class Login {
    public static function yandexAuthonticationToken($code) {
        if(empty($code)){
            return false;
        }
        
        $curl = curl_init();
        
        $data = [
            'grant_type'=>'authorization_code',
            'code' => $code,
            'client_id' => YANDEX_ID,
            'client_secret' => YANDEX_SECRET
        ];       
        $json = json_encode($data);
        
        curl_setopt($curl, CURLOPT_URL,"https://oauth.yandex.ru/token");
        curl_setopt($curl, CURLOPT_POST,1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,"grant_type=authorization_code&code=$code&client_id=".YANDEX_ID."&client_secret=".YANDEX_SECRET);
        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,false);
        try{
            $token = json_decode(curl_exec($curl), true);
            
            return $token;
        } catch (Exception $Login_yandexAuthonticationToken_Exception ){
            return false;
        }catch (ErrorException $Login_yandexAuthonticationToken_ErrorException ){
            return false;
        }catch (ERROR $Login_yandexAuthonticationToken_Error ){
            return false;
        } finally {
            curl_close($curl);
        }
        
        
    }
    
        public static function yandexAuthonticationInfo($token = []) {
            
        if(empty($token['access_token'])){
            return false;
        }
        $url = "https://login.yandex.ru/info?oauth_token={$token['access_token']}";
        $info = json_decode(file_get_contents($url),true);
        // deprecated but time
        return $info;
    }
    
    public static function authorizationInfoFromYandex($info){
        if(empty($info)){
            return false;
        }
        $Users = new Users();
        if(empty($info['default_email'])){
            return false;
        }
        $user = (empty($Users->findUser($info['default_email']))) ? $Users->usersRegisterYandex($info) : $Users->findUser($info['default_email']);
        
        return $user;
    }
}