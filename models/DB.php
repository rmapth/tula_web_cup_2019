<?php

class DB{
    
    protected $pdo;
    
    public function __construct(){
       
        $paramsPath = ROOT . '/config/db-params.php';
        $params = include($paramsPath);
        
        $dsn = "mysql:host={$params['host']};dbname={$params['db']}";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $params['user'], $params['password'],$opt);
        $this->pdo->exec("set names utf8");

    }
    protected function pdoSetSelect($allowed,$source){
        $set = '';
        $values = array();
        if (empty($source)) return false ;
        foreach ($allowed as $field) {
          if (isset($source[$field])) {
            $set.="`".str_replace("`","``",$field)."`". "=:$field AND ";
            $values[$field] = $source[$field];
          }
        }
        $set = trim($set, 'AND ');
        return $set; 
    }
            
    protected function pdoSet($allowed, $source ) {
        $set = '';
        $values = array();
        if (empty($source)) return false ;
        foreach ($allowed as $field) {
          if (isset($source[$field])) {
            $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
            $values[$field] = $source[$field];
          }
        }
        return substr($set, 0, -2); 
    }
    
}